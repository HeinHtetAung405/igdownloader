package com.devshub.igdownloader;

import android.os.Bundle;

import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterShareReceiverActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
    }
}
