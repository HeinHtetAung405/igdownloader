import 'package:flutter/material.dart';
import 'package:igdownloader/tabs/about_tab.dart';
import 'package:igdownloader/tabs/download_tab.dart';
import 'package:igdownloader/tabs/history_tab.dart';
import 'package:igdownloader/utils/navigator.dart';
import 'package:igdownloader/utils/constants.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  List<Widget> _children = [];
  List<Widget> _appBars = [];

  @override
  void initState() {
    _children.add(DownloadTab());
    _children.add(HistoryTab());
    _children.add(AboutTab());

    _appBars.add(_buildAppBar("Download"));
    _appBars.add(_buildAppBar("History"));
    _appBars.add(_buildAppBar("About"));

    super.initState();
  }

  Widget _buildAppBar(String title) {
    return AppBar(
      bottom: PreferredSize(
          child: Container(
            color: Color(0xFF3C3C3C),
            height: 1.0,
          ),
          preferredSize: Size.fromHeight(1.0)),
      automaticallyImplyLeading: false,
      backgroundColor: Color(0xFF3C3F41),
      elevation: 4.0,
      title: Text(
        title,
        style: kToolbarTextStyle,
      ),
      actions: <Widget>[
        IconButton(
            icon: Icon(
              Icons.add_to_home_screen,
              color: Colors.white,
            ),
            onPressed: () => goToIntroPage(context))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBars[_currentIndex],
      body: _children[_currentIndex],
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      onTap: _onTabTapped,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(
              Icons.arrow_downward,
            ),
            title: Text(
                "Download",
              style: kLabelTextStyle,
            )
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.history,
            ),
            title: Text(
                "History",
              style: kLabelTextStyle,
            )
        ),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.info_outline,
            ),
            title: Text(
                "About",
              style: kLabelTextStyle,
            )
        ),
      ],
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentIndex,
    );
  }

  _onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
