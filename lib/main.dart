import 'package:flutter/material.dart';
import 'package:igdownloader/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IGDownloader',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
          primaryColor: Color(0xFF3C3F41),
          scaffoldBackgroundColor: Color(0xFF2A2A2A),
      ),
      home: HomePage(),
    );
  }
}
