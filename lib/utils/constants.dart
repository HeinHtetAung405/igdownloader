import 'package:flutter/material.dart';

var downloadStart = "downloadStart";
var downloadFinish = "downloadFinish";

const fontRegular = 'proximanova';

const kToolbarTextStyle =
TextStyle(color: Colors.white, fontFamily: fontRegular);

const kLabelTextStyle = TextStyle(
  fontFamily: fontRegular
);

const kAboutTextStyle = TextStyle(
  fontFamily: fontRegular,
  fontSize: 18.0
);

const kCompanyTextStyle = TextStyle(
    fontFamily: fontRegular,
    fontSize: 20.0
);

const kIntroTitleTextStyle = TextStyle(
    fontFamily: fontRegular,
    fontSize: 26.0
);

const kIntroDescTextStyle = TextStyle(
    fontFamily: fontRegular,
    fontSize: 20.0
);

