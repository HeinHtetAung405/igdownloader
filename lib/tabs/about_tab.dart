import 'package:flutter/material.dart';
import 'package:igdownloader/utils/constants.dart';

class AboutTab extends StatefulWidget {
  @override
  _AboutTabState createState() => _AboutTabState();
}

class _AboutTabState extends State<AboutTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Container(
              alignment: Alignment.center,
              child: Container(
                width: 110.0,
                height: 110.0,
                child: Image.asset('assets/images/logo.png'),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Meet IGDownloader\n\nA sweet simple and easy to use application for downloading photos and videos from Instagram.'
                  'Save your favorite photos and videos to your device from Instagram to view them anytime.',
              style: kAboutTextStyle,
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              'Brought to you by Devshub',
              style: kCompanyTextStyle,
            ),
            SizedBox(
              height: 8.0,
            ),
            Text(
              'Version 1.0',
              style: kAboutTextStyle,
            )
          ],
        ),
      ),
    );
  }
}
